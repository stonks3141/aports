# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-thunderbolt
pkgver=6.2.0
pkgrel=0
# armhf blocked by qt6-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
pkgdesc="Plasma integration for controlling Thunderbolt devices"
license="GPL-2.0-only OR GPL-3.0-only"
depends="bolt"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	ki18n-dev
	knotifications-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	samurai
	"
checkdepends="
	dbus
	xvfb-run
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/plasma/plasma-thunderbolt.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-thunderbolt-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	dbus-run-session -- xvfb-run -a ctest --test-dir build --output-on-failure -E "(libkbolt-device|kbolt-kded-kded)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
793adf3100e825891035ab89368ac21721a231903f0c1507c96c38706631c958cd0af96a031db08ac200e0a8672996bf96377567246c90563890dc279645054a  plasma-thunderbolt-6.2.0.tar.xz
"
