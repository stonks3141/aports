# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=wacomtablet
pkgver=6.2.0
pkgrel=0
pkgdesc="GUI for Wacom Linux drivers that supports different button/pen layout profiles"
# armhf blocked by qt6-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="xinput"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdoctools-dev
	kglobalaccel-dev
	ki18n-dev
	knotifications-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libwacom-dev
	libplasma-dev
	plasma5support-dev
	qt6-qtbase-dev
	qt6-qtdeclarative-dev
	samurai
	xf86-input-wacom-dev
	"
checkdepends="xvfb-run"
subpackages="$pkgname-lang $pkgname-doc"
_repo_url="https://invent.kde.org/system/wacomtablet.git"
case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/wacomtablet-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run -a ctest --test-dir build --output-on-failure \
		-E "Test.KDED.DBusTabletService"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
573fc2a9dde52aeb127ae76b408db1b58fde17b46dcd99fa9ab1f5fe8eec39849f8c2794cfc099178d0ff3b9bcd0d9a4ce1c2ca7b33a463fbcc6b5528ce66ad2  wacomtablet-6.2.0.tar.xz
"
