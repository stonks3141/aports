# Contributor: Matthias Ahouansou <matthias@ahouansou.cz>
# Maintainer: Matthias Ahouansou <matthias@ahouansou.cz>
pkgname=trunk
pkgver=0.21.0
pkgrel=0
pkgdesc="Build, bundle & ship your Rust WASM application to the web"
url="https://trunkrs.dev"
arch="all !s390x" # nix
license="MIT OR Apache-2.0"
makedepends="
	bzip2-dev
	cargo
	cargo-auditable
	openssl-dev
"
depends="
	rust
	wasm-bindgen
"
source="
	$pkgname-$pkgver.tar.gz::https://github.com/trunk-rs/trunk/archive/refs/tags/v$pkgver.tar.gz
"
options="net"

prepare() {
	default_prepare
	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --frozen -- --skip tools::tests::download_and_install_binaries
}

package() {
	install -Dm 755 target/release/trunk "$pkgdir"/usr/bin/trunk

	for l in APACHE MIT
	do
		install -Dm 644 LICENSE-"$l" "$pkgdir"/usr/share/licenses/"$pkgname"/LICENSE-"$l"
	done
}

sha512sums="
3668cd6cbdeaaa37b7a0e5562365da9dbfd86e12a582b670e0a7613b57ee667a3bbf6776beaa161813cc3539b443b16745f8bd0c750420e9901b41c050cdeb7a  trunk-0.21.0.tar.gz
"
