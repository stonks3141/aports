# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=xdg-desktop-portal-kde
pkgver=6.2.0
pkgrel=0
pkgdesc="A backend implementation for xdg-desktop-portal that is using Qt/KDE"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://phabricator.kde.org/source/xdg-desktop-portal-kde"
license="LGPL-2.0-or-later"
depends="xdg-desktop-portal"
makedepends="
	cups-dev
	extra-cmake-modules
	glib-dev
	kcoreaddons-dev
	kdeclarative-dev
	kio-dev
	kirigami-dev
	kstatusnotifieritem-dev
	kwayland-dev
	libepoxy-dev
	pipewire-dev
	libplasma-dev
	plasma-wayland-protocols
	qt6-qtbase-dev
	samurai
	xdg-desktop-portal-dev
	"
checkdepends="
	dbus-x11
	py3-gobject3
	xvfb-run
	"
subpackages="$pkgname-lang"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/xdg-desktop-portal-kde.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/xdg-desktop-portal-kde-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DKDE_INSTALL_LIBEXECDIR=libexec
	cmake --build build
}

check() {
	# colorschemetest requires the package itself installed
	xvfb-run ctest --test-dir build --output-on-failure -E "colorschemetest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	rm -rf "$pkgdir"/usr/lib/systemd
}

sha512sums="
63f1077ef16e9358c0b881e17d4caf7dd1ef76112a823145c0ad4c17adb38fbc37e36bad0a196bd392804e8335c68ab46ef2e0c67084dafff99926479f508126  xdg-desktop-portal-kde-6.2.0.tar.xz
"
